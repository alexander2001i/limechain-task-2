pragma solidity ^0.6.8;

contract AllowenceSplit {
    address public address1;
    address public address2;
    
    address public creator;
    
     modifier onlyCreator() {
        require(msg.sender == creator, "Only the creator of the contract is authorized to perform this action!");
        _;
    }
    
    constructor (
        address _address1, 
        address _address2
    ) public {
        address1 = _address1;    
        address2 = _address2;
        
        creator = msg.sender;
    }
    
    function changeAddresses(address newAddress1, address newAddress2) public onlyCreator {
        address1 = newAddress1;
        address2 = newAddress2;
    }
    
    function changeAddress(address oldAddress, address newAddress) public onlyCreator {
        if(oldAddress == address1) {
            address1 = newAddress;
        } else if (oldAddress == address2) {
            address2 = newAddress;
        } else {
            revert("The old address in not found!");
        }
    }
    
    function give() payable public {
        require(msg.value > 0);
        
        uint amountToSend = address(this).balance / 2;

        payable(address1).transfer(amountToSend);
        payable(address2).transfer(amountToSend);
    }
}